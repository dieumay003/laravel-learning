<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class UserExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //return User::all();
        $users = User::all();
        $assets_array = array();
        if(!$assets->isEmpty()){
         foreach($users as $user){
             $assets_array[] = array(
             'ID' => $user->id,
             'Name' => $user->name,
             'Email' => $user->email,
             'Date of birth' => $user->dob,
             'Gender' => $user->gender,
             'Role' => $user->role,
             'Updated at' => $user->updated_at
             );
            }
        }
        //dd($assets_array);
        return collect($assets_array);
    }
}
