<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\ProductDetail;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Size;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);
        $data['products'] = $products;
        return view('product/list')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::all();
        $sizes = Size::all();
        $data['categories'] = $categories;
        $data['sizes'] = $sizes;
        return view('product/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        try {
            DB::beginTransaction();
            $this->validate(request(), [
                'name' => 'required',
                'price.*' => 'required|numeric',
                'img' => 'required',
            ]);
            $product = new Product();
            $product->name = $request->name;
            $product->cate_id = $request->category;
            $product->img = $request->image;
            $product->save();
            $product_id = Product::select('id')->limit(1)->orderby('id', 'DESC')->get();
            for ($i = 0; $i < count($request->size); $i++) {
                $price = str_replace(".", "", $request->price[$i]);
                $dataInsert = [
                    'product_id' => $product_id[0]->id,
                    'size_id' => $request->size[$i],
                    'price' => $price
                ];
                $dataInserts[$i] = $dataInsert;
            }
            ProductDetail::insert($dataInserts);

            $product = Product::find($product_id[0]->id);
            $sizes = Size::all();
            $data['product'] = $product;
            $data['sizes'] = $sizes;
            DB::commit();
            return view('product.detail')->with($data);
        }
        catch (Exception $e){
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $sizes = Size::all();
        $data['product'] = $product;
        $data['sizes'] = $sizes;
        return view('product.detail')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $data['product'] = $product;
        return view('product/edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $product = Product::find($id);
            if (!$product) {
                abort(404);
            }
            $product->name = $request->name_product;
            $product->save();
            ProductDetail::where("product_id", $product->id)->delete();
            for ($i = 0; $i < count($request->size); $i++) {
                $price = str_replace(".", "", $request->price[$i]);
                $dataInsert = [
                    'product_id' => $product->id,
                    'size_id' => $request->size[$i],
                    'price' => $price
                ];
                $dataInserts[$i] = $dataInsert;
            }
            ProductDetail::insert($dataInserts);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage());
        }
        return redirect()->back()->with(['success' => 'Data update successful!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $product_id = ProductDetail::find($id)->select('product_id')->get();
            ProductDetail::find($id)->delete();
            if(count($product_id) == 0){
                Product::find($product_id)->delete();
            }
            DB::commit();
        }
        catch (Exception $e){
            DB::rollBack();
            return response()->json(['error'=>true]);
        }
        return response()->json(['error'=>false]);
    }
}
