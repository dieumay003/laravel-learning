<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;

class UserController extends Controller
{
    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        $data['users'] = $users;
        return view('user.list')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->validate(request(), [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required'
            ]);
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->dob = $request->dob;
            $user->gender = $request->gender;
            $user->role = $request->role;

            if(!$request->hasFile("image")) {
                $user->image = $request->image;
            }else{
                $image= $request->file("image");
                $path = $image->store("images", "public");
                $user->image = $path;
            }

            Mail::to($user->email)->send(new WelcomeMail($user));
            $user->save();
            DB::commit();
            return redirect()->back()->with([
                'success' => 'Create user successfully!'
            ]);
        }
        catch (Exception $e){
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage());
        }
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $data['user'] = $user;
        return view('user.detail')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $this->validate(request(), [
                'name' => 'required',
                'email' => 'required|email'
            ]);
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            if($request->password != ""){
                $user->password = Hash::make($request->password);
                dd($user->password);
            }
            $user->dob = $request->dob;
            $user->gender = $request->gender;
            $user->role = $request->role;
            $user->save();
            DB::commit();
            return redirect()->back()->with(['success' => 'Update user successfully!']);
        }
        catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
