<?php

namespace App\Http\Controllers;

use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\Size;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::paginate(10);
        $data['orders'] = $orders;
        return view('order.list')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('order/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'total' => 'required|numeric',
            'address' => 'required',
            'phone' => 'required|digits:10'
        ]);

        $orders = new Order();
        $orders->total = $request->total;
        $orders->address = $request->address;
        $orders->phone = $request->phone;

        $orders->save();
        return redirect()->back()->with('message', 'Data added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $data['order'] = $order;
        return view('order.detail')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orders = Order::find($id);
        $data['order'] = $orders;
        return view('order/edit')->with($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            //tìm order cần update
            $order = Order::find($id);
            //Kiểm tra order tồn tại
            if (!$order) {
                abort(404);
            }

            $total_bill = $order->total;

            $order_detail = OrderDetail::find($request->pk);
            $total_bill -= $order_detail->total;
            $order_detail->qty = $request->value;
            $order_detail->total = $order_detail->qty * $order_detail->product_price;
            $order_detail->save();

            $total_bill +=  $order_detail->total;

            $order->total = $total_bill;
            $order->save();
            DB::commit();
            return response()->json([
               'status' => 'success',
               'totalOrder' => $order->total,
                'idDetail' => $order_detail->id,
                'itemTotal' => $order_detail->total
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
