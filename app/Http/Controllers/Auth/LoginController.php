<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request) {
        //validate
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        // Check thông tin đăng nhập
        if (Auth::attempt($credentials)) {
            //Lưu session đăng nhập
            $request->session()->regenerate();

            //Trả về view sau login
            return redirect()->intended('/')->with(['success' => 'Đăng nhập thành công']);
        }
        //Return trở lại nếu sai thông tin
        return back()->withErrors([
            'message' => 'Không thể đăng nhập, vui lòng kiểm tra lại thông tin đăng nhập'
        ]);

    }

    public function logout() {
        //logout
        Auth::logout();
        session()->flush();
        return redirect()->route('auth.login');
    }
}
