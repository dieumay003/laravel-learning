<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $connection = 'mysql';
    protected $table = 'products';

    public function details() {
        return $this->hasMany(ProductDetail::class, 'product_id', 'id');
    }
}
