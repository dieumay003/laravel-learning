<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    use HasFactory;
    protected $connection = 'mysql';
    protected $table = 'product_details';
    public function size() {
        return $this->hasOne(Size::class, 'id', 'size_id');
    }
}
