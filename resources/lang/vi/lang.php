<?php

return [
    'home' => 'Trang chủ',
    'dashboard' => 'Quản lý',
    'product' => 'Sản phẩm',
    'listproduct' => 'Danh sách sản phẩm',
    'createproduct' => 'Thêm sản phẩm',
    'user' => 'Người dùng',
    'listuser' => 'Danh sách người dùng',
    'createuser' => 'Thêm người dùng',
    'order' => 'Đơn hàng',
    'language' => 'Ngôn ngữ',
    'vi' => 'Tiếng Việt',
    'en' => 'Tiếng Anh',
    'contact' => 'Liên hệ',
    'username' => 'Tên người dùng',
    'pagination' => 'Hiển thị 1-10 bản ghi trong 100 bản ghi',
    'password' => 'Mật khẩu'
];
