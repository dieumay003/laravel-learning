<?php

return [
    'home' => 'Home',
    'dashboard' => 'Dashboard',
    'product' => 'Products',
    'listproduct' => 'List product',
    'createproduct' => 'Create product',
    'user' => 'Users',
    'listuser' => 'List User',
    'createuser' => 'Create User',
    'order' => 'Order',
    'language' => 'Language',
    'vi' => 'Vietnamese',
    'en' => 'English',
    'home' => 'Home',
    'contact' => 'Contact',
    'username' => 'User Name',
    'pagination' => 'Showing 1-10 item in 100 items',
    'password' => 'Password'
];
