
@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">User List</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>User Name</th>
                                <th>Order Id</th>
                                <th>Date Order</th>
                                <th>Address</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($orders as $key => $order)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$order->user->name}}</td>
                                    <td>{{$order->id}}</td>
                                    <td>{{\Carbon\Carbon::parse($order->order_date)->format('d/m/Y')}}</td>
                                    <td>{{$order->receiver_addr}}</td>
                                    <td>{{number_format($order->total, 0, ',', '.')}} VNĐ</td>
                                    <td><span
                                            class="badge {{($order->status== 1? 'badge-primary':$order->status==2)?'badge-warning':'badge-success'}}">{!! \App\Models\Order::$status[$order->status] !!}
                                        </span>

                                    <td>
                                        <a class="btn btn-success" href="{{ route('order.show', $order) }}"
                                           title="Edit"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
    {{$orders->links('Layout.pagelist')}}
@endsection

