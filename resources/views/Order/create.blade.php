@extends('layouts.app')
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item">
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{ route('order.store') }}">
        @csrf()
        <div class="form-row">
        <div class="form-group col-md-6">
        <label for="total">Total</label><br>
            <input type="text" class="form-control" name="total" value="{{ old('total') }}">
        </div>
        <div class="form-group col-md-6">
        <label for="address">Address</label>
            <input type="text" class="form-control" name="address" value="{{ old('address') }}">
        </div>
    </div>
    <div class="form-group col-md-6">
    <label for="phone">Phone</label>
        <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
    </div>
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('orders.index') }}">Back</a>
    </body>
</html>
