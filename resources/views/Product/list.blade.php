@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Product List</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Product Name</th>
                                <th>Product Image</th>
                                <th>Price</th>
                                <th>Size</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $count = 0;
                            @endphp
                            @foreach ($products as $key => $product)
                                @foreach($product->details as $detail)
                                <tr>
                                    <td>{{++$count}}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>
                                        <img src="/storage/app/public/files/1/{{$product->img}}" height="60" width="60">

                                        <img src="{{asset('storage/'.$product->img.'')}}" height="60" width="60">
                                    </td>
                                    <td>{{ number_format($detail->price, 0, ',', '.') }}</td>
                                    <td>{{ $detail->size->name }}</td>
                                    <td>
                                        <a class="btn btn-success" href="{{ route('product.show', $product) }}" title="Edit"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-danger " href="#" onclick="deleteProduct({{$detail->id}})" title="Delete"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                        <div>{{ $products->links() }}</div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
@endsection
@push('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        function deleteProduct(id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete){
                    $.ajax({
                        url: '/product/' + id,
                        method: 'DELETE',
                        data: {
                            _token: '{{csrf_token()}}'
                        },
                        success: function (result){
                            if (!result.error){
                                swal("Good job!", "Item deleted successfully!","success")
                                    .then((value) => {
                                        location.reload();
                                    });
                                return;
                            }
                            swal("Failed!", "Some error occur, please contact administrator!","error");
                        }
                    })
                }else{ swal("Cancel!"); }
            })
        }
    </script>
@endpush
