@extends('layouts.app')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item">
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
<form method="POST" action="{{ route('product.update', $product->id) }}">
    @csrf()
    @method('PUT')
    <div class="form-row">
        <div class="form-group col-md-6">
        <label for="product_code">Product Code</label>
        <input type="text" class="form-control" name="product_code" value="{{ old('product_code') ?? $product->product_code }}">
        </div>
        <div class="form-group col-md-6">
        <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{ old('name') ?? $product->name }}">
        </div>
    </div>
    <div class="form-group col-md-6">
    <label for="description">Description</label>
        <input type="text" class="form-control" name="description" value="{{ old('description') ?? $product->description }}">
    </div><br>
    <div class="form-group col-md-6">
    <label for="description">Price</label>
        <input type="text" class="form-control" name="price" value="{{ old('price') ?? $product->price }}">
    </div><br>
    <div id="hot" class="form-group col-md-6">
            Hot:<br>
            <input type="radio" id="trueHot" name="hot" value="1" {{$product->hot == '1' ? "checked" : "" }}>
            <label for="1">True</label><br>
            <input type="radio" id="falseHot" name="hot" value="0" {{$product->hot == '0' ? "checked" : "" }}>
            <label for="0">False</label><br>
        </div><br>
        <div id="sale" class="form-group col-md-6">
            Sale:<br>
            <input type="radio" id="trueSale" name="sale" value="1" {{$product->sale == '1' ? "checked" : "" }}>
            <label for="1"> True </label><br>
            <input type="radio" id="falseSale" name="sale" value="0" {{$product->sale == '0' ? "checked" : "" }}>
            <label for="0">False</label><br>
        </div>
        <button type="submit">Submit</button>
            <a href="{{ route('products.index') }}">Back</a>
</form>






