@extends('layouts.main')
@section('content')
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert">
            <ul class="list-group">
                @foreach($errors->all() as $error)
                    <li class="list-group-item">
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Create new product</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('product.store') }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="productCode">Product Code</label>
                            <input type="text" value="{{ old('product_code') }}" name="product_cd" class="form-control"
                                   id="productCode" placeholder="Please input product code">
                        </div>
                        <div class="form-group">
                            <label for="name">Product Name</label>
                            <input type="text" value="{{ old('name') }}" class="form-control" id="name"
                                   placeholder="Please input product name">
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> Choose
                                    </a>
                                </span>
                                <input id="thumbnail" class="form-control" type="text" name="filepath">
                            </div>
                            <img id="holder" style="margin-top:15px;max-height:100px;">
                        </div>
                        <div>
                        </div>
                        <div id="add_item">
                            @php $check = true;
                            @endphp
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label for="size">Size</label>
                                    <select class="form-control" name="size[]">
                                        @foreach($sizes as $size)
                                            <option
                                                value="{{ $size->id }}">{{ $size->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script type="text/javascript">
        $('#lfm').filemanager('image');

        $('#thumbnail').change(function(e) {
            $('#holder').attr('src', $(this).val());
        });
        // $('#btn_thumbnail').filemanager('file');
        const max_row = 10;
        const add_item = $("#add_item");

        function click_function() {
            let current_row = $('.form-row').length;
            if (current_row < max_row) {
                add_item.append('<div class="form-row">\n' +
                    '                                    <div class="form-group col-md-2">\n' +
                    '                                        <label for="size">Size</label>\n' +
                    '                                        <select class="form-control" name="size[]">\n' +
                    '                                            @foreach($sizes as $size)\n' +
                    '                                                <option value="{{ $size->id }}" >{{ $size->name }}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group col-md-2">\n' +
                    '                                        <label for="price">Price</label>\n' +
                    '                                        <input type="text" class="form-control price" name="price[]" value="0" />\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group col-md-1 d-flex align-items-end btn_deletes">\n' +
                    '                                        <button type="button" class="btn_delete btn btn-danger btn-outline js-addSize" style="display: block;height: 40px;"><i class="fa fa-minus"></i></button>\n' +
                    '                                    </div>\n' +
                    '                                </div>');
                $(".btn_delete").click(function () {
                    if ($('.form-row').length > 1) {
                        $(this).closest('.form-row').remove();
                    }
                })
            }
        }

        $(document).ready(function () {
            $(".btn_delete").click(function () {
                if ($('.form-row').length > 1) {
                    $(this).closest('.form-row').remove();
                }
            })
        })

        function remove_function() {
            $(".price").attr('value', '');
            $('#productname').attr('value', '');
        }

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endpush
