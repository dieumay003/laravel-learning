@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Product #{{ $product->id }}-{{ $product->name }}</h3>
                    </div>
                    <div class="card-body">
                    <form method="POST" action="{{ route('product.update', $product) }}">
                        @csrf()
                        @method('PUT')
                            <div class="form-group">
                                <label for="productname">Product name</label>
                                <input type="text" class="form-control" name="name" id="productname" value="{{ $product->name }}" />
                            </div>
                            <div id="add_item">
                            @php $check = true; @endphp
                            @foreach($product->details as $detail)
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label for="size">Size</label>
                                    <select class="form-control" name="size[]">
                                        @foreach($sizes as $size)
                                            <option value="{{ $size->id }}" {{ $detail->size_id == $size->id ? "selected" : "" }}>{{ $size->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="price">Price</label>
                                    <input type="text" class="form-control price" name="price[]" value="{{ number_format($detail->price, 0, ',', '.') }}" />
                                </div>
                                @if ($check)
                                    @php $check = !$check; @endphp
                                <div class="form-group col-md-1 d-flex align-items-end">
                                    <button type="button" class="btn btn-success btn-outline js-addSize" style="display: block;height: 40px;" onclick="click_function()"><i class="fa fa-plus"></i></button>
                                </div>
                                @else
                                <div class="form-group col-md-1 d-flex align-items-end">
                                    <button type="button" class="btn_delete btn btn-danger btn-outline js-addSize" style="display: block;height: 40px;"><i class="fa fa-minus"></i></button>
                                </div>
                                @endif
                            </div>
                            @endforeach
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <a class="btn btn-default" href="{{ route('product.index') }}">Back</a>
                                    <button type="button" class="btn btn-default" onclick="reset_function()">Reset</button>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        const add_item = $("#add_item");
        const max = 10;
        $(document).ready(function() {
            $(".btn_delete").click(function () {
                if($('.form-row').length > 1) {
                    $(this).closest('.form-row').remove();
                }
                if($(this).closest('.form-row').hasClass('add-button')){
                    console.log("oke")
                }
            })
        })
        function click_function() {
            let current_row = $('.form-row').length;
            if (current_row < max) {
            $('.add-button').remove();
            add_item.append('<div class="form-row">\n' +
                                '<div class="form-group col-md-2">\n' +
                                    '<label for="size">Size</label>\n' +
                                    '<select class="form-control" name="size[]">\n' +
                                        '@foreach($sizes as $size)\n' +
                                            '<option value="{{ $size->id }}" {{ $detail->size_id == $size->id ? "selected" : "" }}>{{ $size->name }}</option>\n' +
                                        '@endforeach\n' +
                                    '</select>\n' +
                                '</div>\n' +
                                '<div class="form-group col-md-2">\n' +
                                    '<label for="price">Price</label>\n' +
                                    '<input type="text" class="form-control price" name="price[]" value="0" />\n' +
                                '</div>\n' +
                                '<div class="form-group col-md-1 d-flex align-items-end">\n' +
                                    '<button type="button" class="btn_delete btn btn-danger btn-outline js-addSize" style="display: block;height: 40px;"><i class="fa fa-minus"></i></button>\n' +
                                '</div>\n' +
                            '</div>\n');
            $(".btn_delete").click(function () {
                if($('.form-row').length > 1) {
                    $(this).closest('.form-row').remove();
                }
            })
        }
        }
        function reset_function() {
           $(".price").attr('value', '0');
           $('#productname').attr('value', '');
        }
    </script>
  @endpush
