<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>
    <body class="antialiased">
        <nav class="navbar navbar-expand-sm bg-light navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item active">
                <a class="nav-link" href="#">MENU</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href='{{route("product.index")}}'>PRODUCT</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href='{{route("order.index")}}'>ORDER</a>
                </li>
                <li class="nav-item">
                <a class="nav-link " href='{{route("order_detail.index")}}'>ORDER DETAIL</a>
                </li>
            </ul>
        </nav>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
    </body>
</html>
