@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">User #{{ $user->id }}-{{ $user->name }}</h3>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('user.update', $user)}}">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="form-group">
                                    <label>User name</label>
                                    <input type="text" class="form-control reset" name="name"
                                           placeholder="Enter user name" value="{{$user->name}}">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control reset" name="email"
                                           placeholder="Enter email"
                                           aria-describedby="emailHelp" value="{{$user->email}}">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control reset" name="password" id="password"
                                           placeholder="Enter password">
                                </div>
                                <div class="form-group">
                                    <label>Confirm password</label>
                                    <input type="password" class="form-control reset" id="cf_password"
                                           onchange="checkpass()"
                                           placeholder="Enter confirm password">
                                    <div id="error_pass"></div>
                                </div>
                                <div class="form-group">
                                    <label>Day of birth</label>
                                    <input type="date" class="form-control reset" name="dob" placeholder="Enter DOB"
                                           value="{{\Carbon\Carbon::parse($user->dob)->format('Y-m-d')}}">
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gender"
                                               value="1" {{$user->gender = 1 ? 'checked' : ''}} >
                                        <label class="form-check-label">
                                            Male
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gender"
                                               value="0" {{$user->gender = 0 ? 'checked' : ''}}>
                                        <label class="form-check-label">
                                            Female
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control col-md-4" name="role">
                                        <option value="admin">Admin</option>
                                        <option value="manager">Manager</option>
                                        <option value="customer">Customer</option>
                                    </select>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <a class="btn btn-default" href="{{ route('user.index') }}">Back</a>
                                        <button type="button" class="btn btn-default" onclick="remove_function()">Reset
                                        </button>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scrip')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        function remove_function() {
            $(".reset").attr('value', '');
        }

        const password = $('#password');
        const cf_password = $('#cf_password');
        const error_pass = $('#error_pass');

        function checkpass() {
            if (password.val() == cf_password.val()) {
                $("form").on('submit', function (e) {
                    return true;
                })
            } else {
                error_pass.append('</br><div class="alert alert-danger">\n' +
                    '  Re-entered password does not match\n' +
                    '</div>')
                $("form").on('submit', function (e) {
                    return false;
                })

            }
        }
    </script>
@endpush
