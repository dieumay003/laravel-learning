@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">User List</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>DOB</th>
                                <th>Role</th>
                                <th>Action</th>
                                <th>Image</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $key => $user)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{\Carbon\Carbon::parse($user->dob)->format('d/m/Y')}}</td>
                                    <td>{{$user->role}}</td>
                                    <td>
                                        <img src="storage/".$user->image"></img>
                                    </td>
                                    <td>
                                        <a class="btn btn-success" href="{{ route('user.show', $user) }}"
                                           title="Edit"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
    {{$users->links('Layout.pagelist')}}
@endsection
