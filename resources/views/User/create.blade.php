@extends('layouts.main')
@section('content')
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Create new user</h3>
        </div>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item" style="color:#ec4844">
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{route('user.store')}}">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label>User name</label>
                    <input type="text" class="form-control" name="name"
                           placeholder="Enter user name">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Enter email" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="text" class="form-control" name="password" placeholder="Enter password">
                </div>
                <div class="form-group">
                    <label>Day of birth</label>
                    <input type="date" class="form-control" name="dob" placeholder="Enter DOB">
                </div>
                <div class="form-group">
                    <label>Gender</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="gender" value="1" checked>
                        <label class="form-check-label">
                            Male
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="gender" value="0">
                        <label class="form-check-label">
                            Female
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <select class="form-control col-md-4" name="role">
                        <option value="admin">Admin</option>
                        <option value="manager">Manager</option>
                        <option value="customer">Customer</option>
                    </select>
                </div>
                <div>
                    <label>Avatar</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                        </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="image">
                    </div>
                    <img id="holder" style="margin-top:15px;max-height:100px;">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection

@push('script')
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    $('#lfm').filemanager('image');
    var route_prefix = "url-to-filemanager";
    $('#lfm').filemanager('image', {prefix: route_prefix});
</script>
@endpush
