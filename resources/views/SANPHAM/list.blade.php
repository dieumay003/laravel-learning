@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">SANPHAM List</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Country</th>
                                <th>Price</th>
                                <th>Quantity</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($SANPHAMs as $key => $sanpham)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$sanpham->TENSP}}</td>
                                    <td>{{$sanpham->LINKANH}}</td>
                                    <td>{{$sanpham->NUOCSX}}</td>
                                    <td>{{$sanpham->GIA}}</td>
                                    <td>{{$sanpham->SLBAN}}</td>
                                    <td>
                                    <a class="btn btn-success" href="" title="Edit"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
@endsection
