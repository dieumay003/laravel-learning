<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderDetailController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SANPHAMController;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{locale}', function ($locale) {
    if (!in_array($locale, ['en', 'vi'])) {
        abort(404);
    }
    session()->put('locale', $locale);
    return redirect()->back();
});

Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Route::post('/login', [\App\Http\Controllers\Auth\LoginController::class, 'login'])->name('auth.login');


Route::middleware(['auth'])->group(function() {
    Route::group(['middleware'=> 'checkRole:admin'],function(){
        Route::get('/', function () {
            return view('dashboard');
        });
        Route::resource('order', OrderController::class);
        Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('auth.logout');
        Route::delete('/delete/{id}', [ProductController::class, 'destroy']);
        Route::resource('user', UserController::class);
        Route::get('/user/export/', 'UserController@export');
    });
    Route::group(['middleware'=> 'checkRole:manager'],function(){
        Route::resource('product', ProductController::class);
        Route::resource('SANPHAM', SANPHAMController::class);
    });
});
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
