<?php

namespace Database\Seeders;

use Faker\Provider\Address;
use Faker\Provider\en_US\Person;
use Illuminate\Database\Seeder;
use DB;
use Str;
use Faker\Factory;


class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 1; $i <= 25; $i++){
            DB::table('products')->insert([
                'name' => $faker->name,
                'cate_id' => rand(1,5)
            ]);
            for ($j = 1; $j <= rand(1,5); $j++){
                DB::table('product_details')->insert([
                    'product_id' => $i,
                    'size_id' => rand(1,25),
                    'price' => rand(20000,50000)
                ]);
            }
        }


    }
}
