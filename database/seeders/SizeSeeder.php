<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Str;
use Faker\Factory;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        for ($i = 19; $i <= 45; $i++){
            DB::table('sizes')->insert([
                'name' => $i
            ]);
        }
    }
}
