<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'dob' => Carbon::now(),
            'gender' => 1,
            'role' => 'admin'
        ], [
            'name' => 'customer',
            'email' => 'customer@gmail.com',
            'password' => bcrypt('customer'),
            'dob' => Carbon::now(),
            'gender' => 1,
            'role' => 'customer'
        ], [
            'name' => 'manager',
            'email' => 'manager@gmail.com',
            'password' => bcrypt('manager'),
            'dob' => Carbon::now(),
            'gender' => 1,
            'role' => 'manager'
        ]]);
    }
}
